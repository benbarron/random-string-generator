const resultEL = document.querySelector('span.result')
const upperCaseEL = document.querySelector('input.uppercase-checkbox')
const lowerCaseEL = document.querySelector('input.lowercase-checkbox')
const symbolsEL = document.querySelector('input.symbols-checkbox')
const numbersEL = document.querySelector('input.numbers-checkbox')
const submitEL = document.querySelector('button.submit-btn')
const lengthEL = document.querySelector('input.length-input')
const copyEL = document.querySelector('button.copy-to-clipboard-btn')

const getUpperCaseLetter = () => {
  var lower = 65
  var upper = 90
  var rv = Math.floor((upper - lower + 1) * Math.random()) + lower
  return String.fromCharCode(rv)
}

const getLowerCaseLetter = () => {
  var lower = 97
  var upper = 122
  var rv = Math.floor((upper - lower + 1) * Math.random()) + lower
  return String.fromCharCode(rv)
}

const getNumber = () => {
  var lower = 0
  var upper = 9
  var rv = Math.floor((upper - lower + 1) * Math.random()) + lower
  return rv
}

const getSymbol = () => {
  const symbols = '!@#$%^&*()_+{}[]|;:,./?'

  var lower = 0
  var upper = symbols.length - 1

  var rv = Math.floor((upper - lower + 1) * Math.random()) + lower

  return symbols[rv]
}

submitEL.addEventListener('click', e => {
  e.preventDefault()

  var lowerCase = lowerCaseEL.checked
  var upperCase = upperCaseEL.checked
  var symbols = symbolsEL.checked
  var numbers = numbersEL.checked

  if (!lowerCase && !upperCase && !symbols && !numbers) {
    return
  }

  var typesArr = [lowerCase, upperCase, symbols, numbers]

  var typesFunc = n => {
    if (n == 0) {
      return getLowerCaseLetter()
    } else if (n == 1) {
      return getUpperCaseLetter()
    } else if (n == 2) {
      return getSymbol()
    } else {
      return getNumber()
    }
  }

  var length = lengthEL.value
  var password = ''

  for (let i = 0; i < length; i++) {
    var loc = i % 4

    if (typesArr[loc]) {
      password += typesFunc(loc)
    } else {
      length++
    }
  }

  password = password
    .split('')
    .sort(() => 0.5 - Math.random())
    .join('')

  // console.log(password)

  resultEL.innerHTML = password
})

copyEL.addEventListener('click', e => {
  const el = document.createElement('textarea')

  el.value = resultEL.innerHTML
  el.setAttribute('readonly', '')
  el.style.position = 'absolute'
  el.style.left = '-9999px'
  document.body.appendChild(el)

  const selected =
    document.getSelection().rangeCount > 0
      ? document.getSelection().getRangeAt(0)
      : false

  el.select()
  document.execCommand('copy')
  document.body.removeChild(el)

  if (selected) {
    document.getSelection().removeAllRanges()
    document.getSelection().addRange(selected)
  }
})
